import re
import os
import sys

from modalest.utils import read_file, kebab2camel, batch, array_map, tryf
from modalest.distance import pearson_corr_coef, normalized_euclidean
from librosa.feature import mfcc
from itertools import chain
import numpy as np
import scipy.stats
from modalest.plots import custom_violin_plot, custom_lines_plot
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator
import matplotlib.gridspec


def read_audio_mfcc(f, n_mfcc=12, n_mels=40, n_fft=2**14, hop_length=2**8, win_length=2**11, mfcc0=False):
	a, fs = read_file(f)
	a -= np.mean(a)
	A = mfcc(a, fs, n_mfcc=n_mfcc+1-mfcc0, n_mels=n_mels, n_fft=n_fft, hop_length=hop_length, win_length=win_length)[(1-int(mfcc0)):, :]
	return a, fs, A


def select_row(file, key, key_pos=0, sep=",", evaluate=True):
	with open(file, "r") as f:
		for r in map(lambda s: s.split(sep), f):
			if r[key_pos] == key:
				return list(map(tryf(eval), r)) if evaluate else r


def pstars(pvalue, thresholds=(0.05, 0.01, 0.005, 0.001)):
	return "*"*sum(pvalue < t for t in thresholds)


if __name__ == "__main__" and len(sys.argv) > 1:
	dir = sys.argv[1]
	out_file = os.path.join(dir, "eval.csv")
	n_mfcc = 12
	mfcc0 = False
	verbose = True
	show_plot = False
	bw = False

	pearson_corr_coef.__name__ = "pearson_correlation_coefficient"
	normalized_euclidean.__name__ = "normalized_euclidean_dissimilarity"
	features = (pearson_corr_coef, normalized_euclidean)
	feature_domains = ((-1, 1), (0, 1))
	subs = (r"_original\.wav", r"_original.wav", r"_sdtmodaltracker.wav", r"_out.wav")
	classnames = ("SDTModalTracker", "SAMPLE")
	cols = np.array([np.ones(3)*0.75, np.ones(3)*0.33]) if bw else plt.rcParams['axes.prop_cycle'].by_key()['color']
	shapiro_th = 0.05
	
	if os.path.isfile(out_file):
		print("{} has already been saved. Skipping computations".format(out_file))
	else:
		files = list(filter(lambda t: re.search(subs[0], t[0]) is not None and all(os.path.isfile(u) for u in t), map(lambda f: [os.path.join(dir, re.sub(subs[0], s, f)) for s in subs[1:]], os.listdir(dir))))
		header = list(chain(*[["{}_{}".format(f.__name__, i) for i in range(1 - mfcc0, n_mfcc + 1 - mfcc0)] for f in features]))
		with open(out_file, "w") as csv_f:
			# CSV header
			csv_f.write(",".join(chain(["original", "resynthesis"], header)) + "\n")
			
			for t in files:
				if verbose:
					print("Reading files: "+", ".join(f for f in t), end='\r')
				X = [(f, read_audio_mfcc(f, n_mfcc=n_mfcc, mfcc0=mfcc0)) for f in t]
				if verbose:
					print("Read files: "+", ".join(f for f in t)+"   ")
				
				x1_f, (x1, x1_fs, X1) = X[0]
				for x2_f, (x2, x2_fs, X2) in X[1:]:
					csv_f.write(",".join(map(str, chain([os.path.basename(f) for f in (x1_f, x2_f)], *tuple(array_map(lambda m: feat(*m), zip(X1, X2)) for feat in features)))) + "\n")
	
	# Average statistics
	macro_avg, macro_std = [], []
	for i, s in enumerate(subs[2:]):		
		with open(out_file, "r") as csv_f:
			data = array_map(lambda r: [eval(c) for c in r[2:]], filter(lambda r: re.search(s[:-4], r[1]) is not None, map(lambda r: r.split(","), csv_f)))
		macro_avg.append(array_map(np.mean, batch(np.mean(data, axis=0), n_mfcc)))
		macro_std.append(array_map(np.std, batch(np.mean(data, axis=0), n_mfcc)))
	macro_avg, macro_std = np.array(macro_avg), np.array(macro_std)
	for c, avg, std in zip(classnames, macro_avg, macro_std):
		print(c)
		for f, a, s in zip(map(lambda foo: kebab2camel(foo.__name__, 1, 1), features), avg, std):
			print(" ", f, a, "±", s)
	
	# Test
	with open(out_file, "r") as csv_f:
		original_files = sorted(set(r.split(",")[0] for i, r in enumerate(csv_f) if i))
	data = array_map(lambda s: array_map(lambda f: select_row(out_file, f, key_pos=1)[2:], map(lambda f: re.sub(subs[0], s, f), original_files)), subs[2:])
	shapiro_res = array_map(lambda t: array_map(scipy.stats.shapiro, (t[:, f] for f in range(t.shape[-1]))), data)
	ttest_res = scipy.stats.ttest_rel(*data, axis=0)
	wilcoxon_res = array_map(lambda i: scipy.stats.wilcoxon(*data[:, :, i]), range(data.shape[-1]))
	
	# Plot
	fade = 0.5
	spacing = 1/(data.shape[0]+1)
	width = 0.9*spacing
	
	subplots = (2, 1)
	gs = matplotlib.gridspec.GridSpec(*subplots)
	gs.update(left=0.025, right=0.99, top=0.98, bottom=0.12)
	
	legend = []
	ax = None
	for j, (subplot, feat, data_feat, macro_avg_feat, domain_feat, shapiro_feat, ttest_feat, wilcoxon_feat) in enumerate(zip(gs, features, map(lambda a: np.array(a).T, batch(data.T, n_mfcc)), macro_avg.T, feature_domains, map(lambda r: np.array(r).min(axis=1), batch(shapiro_res[:, :, 1].T, n_mfcc)), batch(ttest_res.pvalue, n_mfcc), batch(wilcoxon_res[:, 1], n_mfcc))):
		ax = plt.subplot(subplot, sharex=ax)
		for i, (d, m, c, s) in enumerate(zip(data_feat, macro_avg_feat, cols, classnames)):
			xaxis = np.arange(n_mfcc)+i*spacing
			vp = custom_violin_plot(d, xaxis, widths=width, color=c)['bodies'][0]
			lp = plt.plot(plt.xlim(), np.ones(2)*m, color=c, alpha=fade, zorder=0)[0]
			if not j:
				legend.append((vp, s))
				legend.append((lp, s + "\n(Macro Averaged)"))
		for i, (s, t, w) in enumerate(zip(shapiro_feat, ttest_feat, wilcoxon_feat)):
			plt.text(i + (len(data_feat) - 1)*spacing/2, domain_feat[1], pstars(t) if s > shapiro_th else pstars(w), fontweight=1000 if s > shapiro_th else 0, horizontalalignment='center')
		plt.ylim(domain_feat)
		plt.yticks(np.linspace(*domain_feat, 3))
		plt.gca().yaxis.set_minor_locator(MultipleLocator(np.diff(domain_feat)/4))
		plt.xlim([i*n_mfcc-spacing for i in range(2)])
		plt.xticks(plt.xlim(), [])
		plt.xlabel(kebab2camel(feat.__name__, 1, 1), labelpad=0)
		plt.grid(True, which='both')
	plt.legend(*list(zip(*legend)), ncol=2, loc='upper right', bbox_to_anchor=(1, 0))
	
	plt.gcf().set_size_inches(np.array([21, 9])*0.66)
	plt.savefig(os.path.join(dir, "eval.svg"))
	if show_plot:
		plt.show()
