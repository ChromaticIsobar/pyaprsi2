import os
import sys
import time
import logging
import threading

import modalest.model
import numpy as np


def extract_features(fname, freq_map=None, decay_map=None, magnitude_map=None, thread=None, nthreads=None):
	logging.info("Thread {}/{} starting".format(thread, nthreads))
	x, fs = modalest.model.read_file(fname)
	m = modalest.model.ModalModel(fs=fs, M=2**11, N=2**14, dt=0.2, hf=18000, t60th=0.1, get_intermediate=True)
	m.fit(x)
	sdt_params = m.to_sdt()
	features = np.array([val if foo is None else foo(val) for foo, val in zip([freq_map, decay_map, magnitude_map], sdt_params)])
	
	ofname = "{}.npy".format(fname[:fname.rfind('.')])
	np.save(ofname, (os.path.split(fname)[-1], features, os.path.split(fname)[-1][:(os.path.split(fname)[-1].rfind('.')-2)]))
	logging.info("Thread {}/{} finishing (saved to file '{}')".format(thread, nthreads, ofname))


if __name__ == "__main__" and len(sys.argv) > 1:
	dataset_dir = sys.argv[1]
	map_foos = [lambda f: (modalest.model.hz2mel(f)-modalest.model.hz2mel(20))/(modalest.model.hz2mel(20000)-modalest.model.hz2mel(20)),
	            lambda d: np.minimum(d*3, 1),
	            lambda x: (20*np.log10(x) + 90)/90]
	
	wavfiles = lambda: map(lambda s: os.path.join(dataset_dir, s), filter(lambda f: f[-3:] == "wav", os.listdir(dataset_dir)))
	n = len(list(wavfiles()))
	tic = time.time()
	
	logging.basicConfig(format="%(asctime)s: %(message)s", level=logging.INFO, datefmt="%H:%M:%S")
	threads = [threading.Thread(target=extract_features, args=(fname, *map_foos, i+1, n)) for i, fname in enumerate(wavfiles())]
	for t in threads: t.start()
	for t in threads: t.join()
	
	toc = time.time()
	print("Elapsed {}s".format(int(toc - tic)))
