import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec
from modalest.utils import batch, array_map
from scipy.optimize import curve_fit


def hinge(x, k, q, x0):
	return k*np.minimum(x, x0) + q


if __name__ == "__main__":
	fs = 44100
	H = 64
	dur = 1.5
	t = np.arange(int(fs*dur))/fs
	ktrue, qtrue, t0true = -72.9, -10, 0.9
	xtrue = hinge(t, ktrue, qtrue, t0true)
	
	tnoise = array_map(np.mean, batch(t, H))
	std = np.array(10**(xtrue/20))
	np.random.seed(1)
	xnoise = array_map(lambda a: 20*np.log10(np.mean(a)), batch(10**(xtrue/20) + np.random.randn(std.size)*std, H))
	
	k, q = np.polyfit(tnoise, xnoise, deg=1)
	
	(kstar, qstar, t0star), _ = curve_fit(hinge, tnoise, xnoise, p0=(k, q, t[-1]/2), sigma=10**(xnoise/20), bounds=([-80*fs/H, q, 0], [k, q+20, t[-1]]), method='trf')
	print("{} ~ {}\n{} ~ {}\n{} ~ {}".format(kstar, ktrue, qstar, qtrue, t0star, t0true))
	
	a = 0.2
	gs = matplotlib.gridspec.GridSpec(1, 1)
	gs.update(top=0.98, right=0.98, left=0.02, bottom=0.02)
	plt.subplot(gs[0])
	plt.plot(tnoise, xnoise)
	plt.plot(t, k*t + q, '--', color=np.array([0, 0.5, 1])*(1-a)+a)
	plt.plot(t, hinge(t, kstar, qstar, t0star), '-', color=np.array([1, 0.5, 0])*(1-a)+a)
	plt.xlim([t[0], t[-1]])
	plt.grid(True)
	plt.legend(["Noisy input data", "Linear regression", "Hinge regression"])
	plt.xticks(np.linspace(*plt.xlim(), 5), [])
	plt.yticks(np.linspace(*plt.ylim(), 5), [])
	plt.gcf().set_size_inches(np.ones(2)*4)
	plt.savefig("hingeregression.svg")
