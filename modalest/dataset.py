"""Dataset wrapper with useful functions for the example ML problems"""
import numpy as np
from itertools import chain, compress
import copy
import warnings


def file_name(fname):
	"""Read original file name"""
	return np.load(fname, allow_pickle=True)[0]


def file_data(fname):
	"""Read features data"""
	return np.load(fname, allow_pickle=True)[1]


def file_class(fname):
	"""Read class label"""
	return np.load(fname, allow_pickle=True)[2]


def random_selector(m, n, seed=None):
	"""Generate a boolean selector for m positions out of n
	
	m : int
		number of `True`s to generate
	
	n : int
		length of the array
	
	seed : int
		seed for random number generator (optional)
	
	return : numpy.ndarray
		n-sized boolean array with m `True`s and n-m `False`s"""
	sel = np.zeros(n, bool)
	if seed is not None:
		np.random.seed(seed)
	rnd = np.random.random(m)
	eligible = list(range(n))
	for i, r in enumerate(rnd):
		idx = np.floor((n-i)*r).astype(int)
		sel[eligible[idx]] = True
		del eligible[idx]
	return sel


class Dataset:
	"""Wrapper for a dataset that is not loaded in the memory"""
	class Datalist:
		def __init__(self, paths, read_f=None):
			self.paths = paths
			self.read_f = lambda x: x if read_f is None else read_f(x)
			self.it = None
		
		def __len__(self):
			return len(self.paths)
		
		def __getitem__(self, item):
			return self.read_f(self.paths[item])
		
		def __iter__(self):
			self.it = iter(self.paths)
			return self
		
		def __next__(self):
			try:
				return self.read_f(next(self.it))
			except StopIteration:
				self.it = None
				raise StopIteration()
		
		def __add__(self, other):
			return Dataset.Datalist(list(chain(self.paths, other.paths)), self.read_f)
		
		def __radd__(self, other):
			if isinstance(other, type(self)):
				return self + other
			else:
				warnings.warn("Adding a Datalist and an integer results in ignoring the integer", Warning)
				return self
	
	def __init__(self, files, read_name=file_name, read_data=file_data, read_label=file_class):
		"""files : array-like
			list of file identifiers (only constraint is that they must be acceptable values for all the following read functions)
		
		read_name : callable
			function used to read the original file name of an instance
		
		read_data : callable
			function used to read the feature data of an instance
		
		read_label : callable
			function used to read the class label name of an instance"""
		self.lists = tuple(Dataset.Datalist(files, foo) for foo in (None, read_name, read_data, read_label))
		self.it = None
	
	def __len__(self):
		return len(self.lists[0])
	
	def __getitem__(self, item):
		return tuple(l[item] for l in self.lists)
	
	def __iter__(self):
		self.it = zip(*list(map(iter, self.lists)))
		return self
	
	def __next__(self):
		try:
			return next(self.it)
		except StopIteration:
			self.it = None
			raise StopIteration()
	
	def __add__(self, other):
		res = copy.deepcopy(self)
		# res.lists = tuple(l + m for l, m in zip(self.lists, other.lists))
		with warnings.catch_warnings():
			warnings.simplefilter("ignore")
			res.lists = tuple(map(sum, zip(self.lists, other.lists)))
		return res
		
	def __radd__(self, other):
		if isinstance(other, type(self)):
			return self + other
		else:
			warnings.warn("Adding a Dataset and an integer results in ignoring the integer", Warning)
			return self
	
	def __getattr__(self, item):
		if item == "classes":
			return set(iter(self.lists[-1]))
		else:
			return super(Dataset, self).__getattr__(item)
	
	def filterclass(self, label, keep=True):
		"""Return a dataset with only the instances of the specified class
		
		label : 
			the class label
		
		keep : bool
			whether to keep elements of the specified class (default, `True`) or to discard them (`False`)"""
		return Dataset([x[0] for x in self if (x[-1] == label) == keep], *[l.read_f for l in self.lists[1:]])
	
	def partition(self, p, seed=None):
		"""Partition the dataset in two datasets keeping the class balance
		
		p : double
			fraction of elements in the first returned dataset (must be between 0 and 1)
	
		seed : int
			seed for random number generator (optional)
		
		return : Dataset, Dataset
			a dataset with a fraction of `p` of the elements of the original dataset and a dataset with a fraction of `1-p` of the elements of the original dataset"""
		classes = self.classes
		if len(classes) > 1:
			partitions = [self.filterclass(c).partition(p, seed=seed) for c in classes]
			with warnings.catch_warnings():
				warnings.simplefilter("ignore")
				return sum(par[0] for par in partitions), sum(par[1] for par in partitions)
		elif len(classes) < 1:
			raise RuntimeError("No labels found in dataset")
		else:
			selectors = random_selector(np.round(p*len(self)).astype(int), len(self), seed=seed)
			return Dataset([x[0] for x in compress(self, selectors)], *[l.read_f for l in self.lists[1:]]), Dataset([x[0] for x in compress(self, np.logical_not(selectors))], *[l.read_f for l in self.lists[1:]])
