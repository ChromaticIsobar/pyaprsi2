"""Distance functions ([reference](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.494.5172&rep=rep1&type=pdf))"""
from itertools import product, chain
from scipy.spatial import distance
import numpy as np


def foomatrix(foo, a, b):
	"""Compute foo(a[i], b[j]) for every a[i] in a and b[j] in b
	
	foo : callable
		the function
	
	a, b : numpy.ndarray
	
	return : numpy.ndarray
		matrix m so that m[i, j] = foo(a[i], b[j])"""
	m = np.zeros((a.shape[1], b.shape[1]))
	for i, j in product(range(a.shape[1]), range(b.shape[1])):
		m[i, j] = foo(a[:, i], b[:, j])
	return m


def average_linkage(A, B, d=distance.euclidean):
	"""Compute the *average linkage* distance
	
	A, B : numpy.ndarray
		sets of n-dimensional points so that A.shape[0] == B.shape[0] == n
	
	d : callable
		the distance metric to use between each couple of points (defaults to `euclidean` distance)
	
	return : double
		the distance between A and B"""
	return sum(d(A[:, i], B[:, j]) for i, j in product(range(A.shape[1]), range(B.shape[1])))/(A.shape[1]*B.shape[1])


def smd(A, B, d=distance.euclidean):
	"""Compute the *sum of minimum distances* distance
	
	A, B : numpy.ndarray
		sets of n-dimensional points so that A.shape[0] == B.shape[0] == n
	
	d : callable
		the distance metric to use between each couple of points (defaults to `euclidean` distance)
	
	return : double
		the distance between A and B"""
	dmatrix = foomatrix(d, A, B)
	return sum(chain((min(dmatrix[i, :]) for i in range(A.shape[1])), (min(dmatrix[:, j]) for j in range(B.shape[1]))))/(A.shape[1]+B.shape[1])


def hausdorff(A, B, d=distance.euclidean):
	"""Compute the *hausdorff* distance
	
	A, B : numpy.ndarray
		sets of n-dimensional points so that A.shape[0] == B.shape[0] == n
	
	d : callable
		the distance metric to use between each couple of points (defaults to `euclidean` distance)
	
	return : double
		the distance between A and B"""
	dmatrix = foomatrix(d, A, B)
	return max(max(min(dmatrix[i, j] for j in range(B.shape[1])) for i in range(A.shape[1])), max(min(dmatrix[i, j] for i in range(A.shape[1])) for j in range(B.shape[1])))


def ribl(A, B, d=distance.euclidean):
	"""Compute the *RIBL* distance
	
	A, B : numpy.ndarray
		sets of n-dimensional points so that A.shape[0] == B.shape[0] == n
	
	d : callable
		the distance metric to use between each couple of points (defaults to `euclidean` distance)
	
	return : double
		the distance between A and B"""
	dmatrix = foomatrix(d, A, B)
	if dmatrix.shape[0] > dmatrix.shape[1]:
		dmatrix = dmatrix.T
	return sum(min(dmatrix[i, :]) for i in range(dmatrix.shape[0]))/dmatrix.shape[1]


def pearson_corr_coef(A, B):
	"""Compute the *Pearson correlation coefficient*
	
	A, B : numpy.ndarray
		arrays of the same shape
	
	return : double
		the Pearson correlation coefficient"""
	return np.dot(A - np.mean(A), B - np.mean(B))/((A.size - 1)*np.std(A)*np.std(B))


def normalized_euclidean(A, B):
	r"""Compute the *normalized Euclidean dissimilarity*
	
	A, B : numpy.ndarray
		arrays of the same shape
	
	return : double
		the dissimilarity between A and B, normalized by the double of the sum of squared norms of A and B"""
	return np.sqrt(np.sum(np.abs(A - B)**2) / (2*(np.sum(np.abs(A)**2) + np.sum(np.abs(B)**2))))
