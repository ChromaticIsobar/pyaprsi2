"""ML algorithms and models"""

import numpy as np
from scipy.spatial import distance


class KNN:
	"""KNN classifier"""
	def __init__(self, data, labels, k=1, d=distance.euclidean):
		"""data : array-like
			feature instances
		
		labels : array-like
			class labels for each data instance
		
		k : int
			number of nearest neighbours
		
		d : callable
			distance metric (defaults to `euclidean`)"""
		self.data = data
		self.labels = labels
		self.k = k
		self.d = d
		self.classes = list(set(labels))
	
	def __call__(self, x):
		"""Classify instance x"""
		neighbours_d = np.array([self.d(x, y) for y in self.data])
		kn_classes = []
		for i in range(self.k):
			mini = np.argmin(neighbours_d)
			kn_classes.append(self.labels[mini])
			neighbours_d[mini] = np.inf 
		return sorted(((c, kn_classes.count(c)) for c in set(kn_classes)), key=lambda x: -x[1])[0][0]


def rbf(gamma):
	"""Return the radial basis function with the specified paramater gamma
	
	gamma : double
		parameter of the rbf function
	
	return : function
		the rbf function"""
	def inner_rbf(x):
		return np.exp(-gamma*x*x)
	inner_rbf.__name__ = "rbf_{}".format(gamma)
	return inner_rbf
