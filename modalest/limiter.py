"""Implementation of a simple limiter"""
import numpy as np


def rms(a):
	"""Compute the root-mean-square value of array `a`
	
	a : numpy.ndarray
		the array of which to compute the rms
	
	return : double
		the rms value"""
	return np.sqrt(np.mean(a**2))


def amp(a):
	"""Compute the peak amplitude value of array `a`
	
	a : numpy.ndarray
		the array of which to compute the peak amplitude
	
	return : double
		the peak amplitude"""
	return np.max(np.abs(a))


class Limiter:
	"""Implementation of a simple limiter"""
	def __init__(self, detect=1, release=0, attack=0, threshold=1, peak=True):
		self.attack = attack
		self.release = release
		self.detect = detect
		self.delay_line = np.zeros(detect)
		self.delay_i = 0
		self.threshold = threshold
		self.runpeak = 0
		self.gain = 1
		self.envelope_f = amp if peak else rms

	def __call__(self, *args, **kwargs):
		return self.process(*args, **kwargs)
	
	def __getattr__(self, key):
		return self.__dict__[key] if key in self.__dict__.keys() else getattr(self, "__{}__".format(key))()
	
	def process(self, x):
		y = np.zeros((*x.shape,))
		
		for i, v in enumerate(x):
			self.delay_line[self.delay_i] = v
			self.delay_i = (self.delay_i + 1) % self.detect
			
			self.runpeak = max(self.envelope, self.runpeak*self.release)
			tgain = self.threshold/self.runpeak if self.runpeak > self.threshold else 1
			self.gain = self.gain*self.attack + tgain*(1 - self.attack) if tgain < self.gain else tgain 
				
			# y[i] = np.array([self.envelope, self.runpeak, self.threshold/self.gain, self.envelope*self.gain, self.delay_line[int(self.delay_i + self.detect/2) % self.detect]*self.gain])
			y[i] = self.delay_line[int(self.delay_i + self.detect/2) % self.detect]*self.gain
		
		return y
	
	def __envelope__(self):
		return self.envelope_f(self.delay_line)
