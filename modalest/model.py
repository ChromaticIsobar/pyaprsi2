"""Data-driven modelling of modal impact sounds"""
from modalest.utils import *
from itertools import filterfalse, chain, compress, count, islice, cycle, combinations
from functools import reduce
import sms.models.spsModel as sps
import scipy.signal
from scipy.optimize import curve_fit
import warnings


def split_tracks(freq_tracks, mag_tracks):
	"""Split non-contiguous SMS tracks
	
	freq_tracks, mag_tracks : numpy.ndarray
		tracks derived from SMS analysis"""
	def split_one_track(freq_track, mag_track):
		y = np.diff(np.equal(freq_track, 0).astype(int)).astype(bool); y[-1] = True
		idx = np.array(list(compress(count(), y)))
		
		def split_(t):
			return np.transpose(np.array([np.concatenate((np.zeros(i), zz, np.zeros(t.size - zz.size - i))) for i, zz in zip(chain([0], idx[:-1]), np.split(t, idx))]))
		
		zf, zm, = split_(freq_track), split_(mag_track)
		counter = np.sum(np.array([np.sum(np.greater(zf[:, list(islice(cycle(sel), zf.shape[1]))], 1), axis=1) for sel in [[True, False], [False, True]]]), axis=1)
		with np.errstate(invalid="ignore", divide="ignore"):
			takeodd, takeeven = counter[0]/counter[1] < 0.5, counter[1]/counter[0] < 0.5
			
		def select(z):
			return z[:, list(islice(cycle([takeeven, takeodd]), z.shape[1]))]
		
		return (select(zf), select(zm)) if takeodd or takeeven else None
	
	Z = tuple(filterfalse(lambda a: a is None, (split_one_track(freq_tracks[:, i], mag_tracks[:, i]) for i in range(freq_tracks.shape[1]))))
	return np.concatenate(tuple(zf for zf, _ in Z), axis=1), np.concatenate(tuple(zm for _, zm in Z), axis=1)


def group_tracks(freq_tracks, mag_tracks, df, verbose=False):
	"""Group SMS non superimposed tracks with similar frequencies
	
	freq_tracks, mag_tracks : numpy.ndarray
		tracks derived from SMS analysis
	
	df : function
		frequency mapping function"""
	
	def in_range(ft1, ft2):
		return np.abs(df(mean_freq(ft1)) - df(mean_freq(ft2))) < 1

	def non_superimposed(ft1, ft2):
		nz1, nz2 = np.nonzero(ft1)[0], np.nonzero(ft2)[0]
		return all(nz not in nz2 for nz in nz1)
	
	def mergeable(ft1, ft2):
		return in_range(ft1, ft2) and non_superimposed(ft1, ft2)
	
	def merge_rc(m, idx, axis=0, merge_f=np.add):  # axis=0 to merge rows, axis=1 to merge cols, etc...
		def get_slice(i):
			return tuple(i if d == axis else slice(s) for d, s in enumerate(m.shape))
		n = np.reshape(reduce(merge_f, (m[get_slice(i)] for i in idx)), newshape=tuple((-1)**(d != axis) for d, _ in enumerate(m.shape)))
		keep_indices = list(filterfalse(lambda h: h in idx, range(m.shape[axis])))
		return np.concatenate((m[get_slice(keep_indices)], n), axis=axis)
	
	def merge_cols(m, idx, merge_f=np.add):
		return merge_rc(m, idx, merge_f=merge_f, axis=1)
	
	def merge_rows(m, idx, merge_f=np.add):
		return merge_rc(m, idx, merge_f=merge_f, axis=0)
	
	flag = True
	start_i = 0
	w = ndigits(freq_tracks.shape[1])
	while flag:
		if verbose:
			print("    track count: {t: >{w}}".format(w=w, t=freq_tracks.shape[1]), end='\r')
		flag = False
		for i, j in combinations(range(start_i, freq_tracks.shape[1]), 2):
			start_i = i
			if mergeable(freq_tracks[:, i], freq_tracks[:, j]):
				freq_tracks, mag_tracks, = merge_cols(freq_tracks, (i, j)), merge_cols(mag_tracks, (i, j))
				flag = True
				break
	return freq_tracks, mag_tracks


def discard_tracks(freq_tracks, mag_tracks, dt):
	"""Drop SMS tracks that start dt frames after the start of the first-starting track
	
	freq_tracks, mag_tracks : numpy.ndarray
		tracks derived from SMS analysis
		
	dt : int
		tolerance"""
	I = [np.nonzero(freq_tracks[:, i])[0][0] for i in range(freq_tracks.shape[1])]
	ifirst = min(I)
	
	def filter_freqs(z):
		return np.concatenate(tuple(np.reshape(z[:, i], newshape=(-1, 1)) for i in range(z.shape[1]) if I[i] < ifirst + dt), axis=1)
	freq_tracks, mag_tracks = filter_freqs(freq_tracks), filter_freqs(mag_tracks)
	return freq_tracks, mag_tracks


def regression_tracks_stft(x, fs, freq_tracks, nc, th=-60, verbose=False, wf=5):
	"""Estimate parameters from multiresolution STFT analysis
	
	x : numpy.ndarray
		array of audio samples
	
	fs : double
		sampling frequency
	
	freq_tracks : numpy.ndarray
		frequency tracks derived from SMS analysis
	
	nc : int
		number of cycles per window
	
	th : double
		threshold (in dB) below which to stop magnitude analysis (default is `-60`)
	
	verbose : bool
		verbosity setting (default is `False`)
	
	wf : int
		number of digits for frequency values (for printing, default is `5`)
	
	return : f, m, k
		modal frequencies, magnitudes (in dB) and decays (20 times the coefficient of the exponent)"""
	freqs = np.array([mean_freq(freq_tracks[:, i]) for i in range(freq_tracks.shape[1])])
	mags, ks = np.zeros(freqs.shape), np.zeros(freqs.shape)
	
	w = ndigits(freqs.size)
	for i, f in enumerate(freqs):
		if verbose:
			print("    track: {i: >{w}}/{t} {c} {f: >{wf}} Hz".format(c=u"\u2248", i=i + 1, w=w, t=freqs.size, f=int(f), wf=wf), end='\r')
		nperseg = np.round(nc*fs/f).astype(int)  # number of samples per window, corresponding to nc cycles at frequency f
		nwins = np.floor(x.size/nperseg).astype(int)  # number of windows of length ns, crop on tail
		
		phasor = np.reshape(np.exp(-2j*np.pi*f*np.arange(nperseg)/fs), (1, -1))  # row vector to perform single-frequency STFT
		xbuf = np.array([[x[w*nperseg + i] for w in range(nwins)] for i in range(nperseg)])  # buffer matrix to perform STFT
		
		f_stft = np.dot(phasor, xbuf).flatten()  # single-frequency STFT at frequency f
		taxis = np.arange(nwins)*nperseg/fs  # beginning time instant of each window
		
		f_mag = 20*np.log10(np.maximum(np.abs(f_stft), 10**((th - 3)/20))*2/nperseg)
		wth = next(filterfalse(lambda i: i != f_mag.size and f_mag[i] > th, range(2, f_mag.size + 1)))  # wth is the number of initial windows where the magnitude is above the threshold 
		
		ks[i], mags[i] = np.polyfit(taxis[:wth], f_mag[:wth], deg=1)
		
	print("    track: {i: >{w}}/{t}   {f: >{wf}}   ".format(i=i + 1, w=w, t=freqs.size, f="", wf=wf))
	return freqs, mags, ks


def regression_tracks_sms(freq_tracks, mag_tracks, fs, H, knee=True):
	"""Estimate parameters from SMS magnitude tracks
	
	freq_tracks, mag_tracks : numpy.ndarray
		tracks derived from SMS analysis
	
	fs : double
		sampling frequency
	
	H : int
		hop size
	
	return : f, m, k
		modal frequencies, magnitudes (in dB) and decays (20 times the coefficient of the exponent)"""
	freqs = np.array([mean_freq(freq_tracks[:, i]) for i in range(freq_tracks.shape[1])])
	if knee:
		def hinge(x, k, q, x0):
			return k*np.minimum(x, x0) + q
	
	def kq(j):
		x = np.nonzero(mag_tracks[:, j])[0]
		y = mag_tracks[x, j]
		x = x*H/fs
		k, q = np.polyfit(x, y, deg=1)
		if knee:
			try:
				with warnings.catch_warnings():
					warnings.simplefilter("ignore")
					(kstar, qstar, _), _ = curve_fit(hinge, x, y, p0=(k, q, x[-1]/2), sigma=10**(y/10), bounds=([-80*fs/H, q, 0], [k, q+20, x[-1]]), method='trf') 
			except RuntimeError:
				kstar, qstar = k, q
			k, q = kstar, qstar
		return k, q
	
	ks, mags = np.array([kq(i) for i in range(mag_tracks.shape[1])]).T
	return freqs, mags, ks


class ModalModel(object):
	"""This class represents models to estimate the modal parameters of impacted resonators"""
	def __init__(self, w='hamming', M=2048, N=2048, H=256, t=-80, minSineDur=0.02, maxnSines=64, freqDevOffset=10, freqDevSlope=0.001, stocf=0.2, get_intermediate=False, df=hz2mel, dt=None, ncycles=12, lf=20, hf=20000, mth=-120, mabs=False, t60th=0, regression_f='regression_tracks_sms', reverse=True, verbose=False, **kwargs):
		"""w : str
			type of window used for STFT in SMS analysis (default is `'hamming'`)
		
		M : int
			length of window used for STFT in SMS analysis (default is `2048`)
		
		N : int
			length of fft used for STFT in SMS analysis (default is `2048`)
		
		H : int
			length of hop used for STFT in SMS analysis (default is `256`)
		
		t : double
			magnitude threshold (in dB) for discarding partials in SMS analysis (default is `-80`)
		
		minSineDur : double
			minimum sine duration (in seconds) for discarding partials in SMS analysis (default is `0.02`)
		
		maxnSines : int
			maximum number partials per frame in SMS analysis (default is `64`)
		
		freqDevOffset : double
			minimum frequency deviation at 0Hz in SMS analysis (default is `10`)
		
		freqDevSlope : double
			slope increase of minimum frequency deviation in SMS analysis (default is `0.001`)
		
		stocf : double
			decimation factor used for the stochastic approximation in SMS analysis (default is `0.2`), note: it doesn't have any effect in the current version of the class
		
		get_intermediate : bool
			if `True`, save also intermediate analysis results when fitting in the field `self.intermediate` (default is `False`)
		
		df : function
			frequency mapping function for grouping non-concurrent partials: partials f1 and f2 are grouped if `|df(f1) - df(f2)| < 1` (default is `hz2mel`)
		
		dt : double
			time delay threshold (in seconds) for discarding partials not starting with the earliest-starting partial (default is `None` --> `2*self.minSineDur`)
		
		lf : double
			minimum acceptable modal frequency (default is `20`)
		
		hf : double
			maximum acceptable modal frequency (default is `20000`)
		
		mth : double
			minimum acceptable starting magnitude (in dB, default is `-120`)
		
		mabs : boolean
			whether starting magnitude is absolute or relative (default is `False`, i.e. relative)
		
		t60th : double
			minimum acceptable t60 (in seconds, default is `0`)
			
		regression_f : str
			name of the function to use for regression (default is `'regression_tracks_sms'`). Supported functions are:
			`['regression_tracks_sms', 'regression_tracks_stft']`
		
		reverse : bool
			whether to perform SMS analysis in reverse (default is `True`)
		
		verbose : bool
			whether to print verbose information while fitting (default is `False`)"""
		for k, v in filterfalse(lambda x: x[0] in ['self', 'kwargs'], chain(locals().items(), kwargs.items())):
			self.__dict__[k] = v
	
	def synth(self, n, fs=None, p=None):
		"""Synthesize audio as the sinusoidal part of an impact's sound
		
		n : int
			number of samples
		
		fs : int
			sampling frequency, defaults to the previously defined instance's sampling frequency
		
		p : numpy.ndarray
			array of initial phases of all modes, defaults to gaussian random IID samples
		
		return : np.ndarray
			array of audio samples"""
		fs = self.fs if fs is None else fs
		taxis = np.arange(n)/fs
		p = np.random.randn(self.f.size) if p is None else p
		return sum(10**((self.m[i] + self.k[i]*taxis)/20) * np.cos(2*np.pi*self.f[i]*taxis + p[i]) for i in range(self.f.size))
	
	def fit(self, x, **kwargs):
		"""Fit the model to the audio example x
		
		x : np.ndarray
			array of audio samples
		
		**kwargs : 
			any parameter of the model can be specified here as a named argument. For a list of available parameters see the constructor
		
		return : ModalModel
			the fitted model"""
		for k, v in kwargs.items():
			self.__dict__[k] = v
		self.dt = 2*self.minSineDur if self.dt is None else self.dt
		
		if self.verbose:
			print("Fitting model:\n  Parameters:\n{}".format("\n".join("    {}".format(s) for s in print_parameters(self).split("\n"))))
		freqs, mags = self.sms_model(x)
		
		if self.verbose:
			print("  Linear regression:")
		if self.regression_f == 'regression_tracks_stft':
			self.f, self.m, self.k = regression_tracks_stft(x, self.fs, freqs, nc=self.ncycles, verbose=self.verbose)
		elif self.regression_f == 'regression_tracks_sms':
			self.f, self.m, self.k = regression_tracks_sms(freqs, mags, self.fs, self.H)
		else:
			raise ValueError("No regression function supported for name '{}'".format(self.regression_f))
		self.clean_modal_features()
		
		return self
	
	def sms_model(self, x):
		"""Analyse the audio with SMS and preprocess
		
		x : np.ndarray
			array of audio samples
		
		return : tuple of numpy.ndarray
			frequencies, magnitudes and phases of the found sinusoidal tracks"""
		w = scipy.signal.get_window(self.w, self.M)
		
		if self.verbose:
			print("  Analysing audio...", end='\r')
		res = sps.spsModelAnal(x=np.flip(x) if self.reverse else x, fs=self.fs, w=w, N=self.N, H=self.H, t=self.t, minSineDur=self.minSineDur, maxnSines=self.maxnSines, freqDevOffset=self.freqDevOffset, freqDevSlope=self.freqDevSlope, stocf=self.stocf)
		if self.reverse:
			res = list(map(np.flipud, res))
		if self.get_intermediate:
			self.intermediate = [res]
		if self.verbose:
			print("  Analysed audio    \n  Splitting tracks...", end='\r')
		res = split_tracks(*res[:2])
		if self.get_intermediate:
			self.intermediate.append(res)
		if self.verbose:
			print("  Split tracks       \n  Grouping tracks:")
		res = group_tracks(*res, self.df, verbose=self.verbose)
		if self.get_intermediate:
			self.intermediate.append(res)
		if self.verbose:
			print("\033[1A  Grouped tracks: \n\n  Discarding tracks...", end="\r")
		res = discard_tracks(*res, self.dt*self.fs/self.H)
		if self.get_intermediate:
			self.intermediate.append(res)
		if self.verbose:
			print("  Discarded tracks:    \n    track count: {}".format(res[0].shape[1]))
		return res
	
	def clean_modal_features(self):
		"""Discard modal features that do not satisfy the following constraints:
		
		- the decay parameter `k` must be negative
		
		- the modal frequency must be within the range [`lf`, `hf`]"""
		if self.verbose:
			print("  Cleaning features...", end='\r')
		flags = reduce(lambda x, y: np.logical_and(x, y), (np.less(self.k, 0), np.greater_equal(self.f, self.lf), np.less_equal(self.f, self.hf), np.less_equal(self.m, 0), np.greater_equal(-60/self.k, self.t60th)))
		flags = np.logical_and(flags, np.greater_equal(self.m, (0 if self.mabs else np.max(self.m[flags])) + self.mth))
		self.f, self.m, self.k = self.f[flags], self.m[flags], self.k[flags]
		idx = np.argsort(self.f)
		self.f, self.m, self.k = self.f[idx], self.m[idx], self.k[idx]
		if self.verbose:
			print("  Cleaned features:    \n    track count: {}".format(self.f.size))
	
	def to_sdt(self, fname=None):
		"""Get a SDT Resonator parameters equivalent to the model
		
		fname : str
			file name (full path) of output file (default is `None` --> no output file)
		
		return : tuple
			the parameters in the format `(frequencies, decays, gains, fragment_size)` where `fragment_size == 1`."""
		magnitudes = 10**(self.m/20)
		decays = -40*np.log10(np.e)/self.k
		
		if fname is not None:
			sdt_write_to_file(fname, self.f, decays, magnitudes)
		
		return self.f, decays, magnitudes, 1
	
	def sms_tracks(self, n):
		"""Get SMS-track-like structures from the model's parameters for comparison
		
		n : int
			number of frames
			
		return : tuple
			frequency and magnitude tracks as if derived from SMS analysis"""
		taxis = np.arange(n)*self.H/self.fs
		return np.tile(np.reshape(self.f, newshape=(1, -1)), reps=(n, 1)), np.array([self.m[i] + self.k[i]*taxis for i in range(self.f.size)]).T
