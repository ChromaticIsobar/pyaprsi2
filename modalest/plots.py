"""Functions used for plotting analysis results"""
import matplotlib.pyplot as plt
from matplotlib.collections import LineCollection
from modalest.utils import *
from scipy.signal import stft
from functools import reduce
import warnings


def plot_alpha(x, y, alpha, cmap='gray'):
	"""Plot lines with varying alpha
	
	x : numpy.ndarray
		x coordinates of line points
	
	y : numpy.ndarray
		y coordinates of line points
	
	alpha : numpy.ndarray
		alpha values of line points
	
	cmap : str
		colormap"""
	def plot_line_alpha(x, y, c):
		idx = np.arange(y.size)[np.logical_not(np.isnan(y))]
		x, y, c = x[idx], y[idx], (c[idx[:-1]] + c[idx[1:]])/2
		points = np.array([x, y]).T.reshape(-1, 1, 2)
		segments = np.concatenate([points[:-1], points[1:]], axis=1)
		lc = LineCollection(segments, cmap=cmap)
		lc.set_array(c)
		plt.gca().add_collection(lc)
	
	alpha = 1 - normalize(alpha)
	for i in range(y.shape[1]):
		plot_line_alpha(x, y[:, i], alpha[:, i])


def plot_freq_and_mag(freq_tracks, mag_tracks, stft_=True, x=None, fs=None, win=None, M=None, H=None, N=None, ylim=(-80, 0)):
	"""Plot SMS frequency and magnitude tracks
	
	freq_tracks, mag_tracks : 
		frequency and magnitude tracks derived from SMS analysis
	
	stft_ : bool
		if True, plot the spectrogram too
	
	x, fs, win, M, H, N : 
		parameters of the stft"""
	ax = plt.subplot(221)
	taxis = np.arange(freq_tracks.shape[0])*H/fs
	plt.plot(taxis, no_zeros(freq_tracks))
	plt.subplot(222, sharex=ax)
	plt.plot(taxis, no_zeros(mag_tracks))
	plt.ylim(ylim)
	plt.subplot(223, sharex=ax, sharey=ax) if stft_ else plt.subplot(212, sharex=ax, sharey=ax)
	plot_alpha(taxis, no_zeros(freq_tracks), no_zeros(mag_tracks))
	
	if stft_:
		plt.subplot(224, sharex=ax, sharey=ax)
		spectrogram(x, fs, win, M, H, N)


def spectrogram(x, fs, win, M, H, N):
	xfaxis, xtaxis, Xtf = stft(x, fs, window=win, nperseg=M, noverlap=(M-H), nfft=N)
	Ximg = np.abs(Xtf); Ximg = 20*np.log10(np.clip(Ximg/np.max(Ximg), 10**(-6), 1)); Ximg = Ximg - np.min(Ximg); Ximg = Ximg/np.max(Ximg)
	plt.imshow(Ximg, origin='lower', extent=(xtaxis[0], xtaxis[-1], xfaxis[0], xfaxis[-1]), aspect='auto')


def custom_violin_plot(dataset, positions=None, widths=0.5, showmeans=False, showmedians=True, showbox=True, points=100, color=None, alpha=1, dotfacecolor='w', dotedgecolor='k', boxtichkness=3, boxcolor='k'):
	"""Plot a customized violin plot. For arguments `dataset`, `positions`, `widths`, `showmeans`, `showmedians` and `points` see matplotlib.pyplot.violinplot. In this function, `showmeans` and `showmedians` are mutually exclusive and `showmeans=True` overrides `showmedians=True`.
	
	color : color specification
		body color of the violins
	
	alpha : float
		body transparency of the violins
	
	dotfacecolor : color specification
		the color of the face of the mean/median markers (default is `'w'`)
	
	dotedgecolor : color specification
		the color of the edge of the mean/median markers (default is `'k'`)
	
	showbox : bool
		whether to show the quartile box (default is `True`)
	
	boxthickness : int
		width of the quartile box (default is `3`)
	
	boxcolor : color specification
		color of the quartile box (default is `'k'`)"""
	vp = plt.violinplot(dataset=dataset, positions=positions, widths=widths, showmeans=False, showextrema=False, showmedians=False, points=points)
	for body in vp['bodies']:
		if color is not None:
			body.set_facecolor(color)
		body.set_alpha(alpha)
	q = np.percentile(dataset, [25, 50, 75], axis=0)
	if showmeans:
		q[1, :] = np.mean(dataset, axis=0)
		if showmedians:
			warnings.warn("Cannot show both means and medians. Means are shown", UserWarning)
	if showmedians or showmeans:
		plt.scatter(positions, q[1, :], marker='o', color=dotfacecolor, edgecolor=dotedgecolor, s=30, zorder=3)
	if showbox:
		plt.vlines(positions, q[0, :], q[2, :], color=boxcolor, linestyle='-', lw=boxtichkness)
	return vp


def custom_lines_plot(x, y, linespec='-', **kwargs):
	"""Plot lines described by x and y as discontinuous lines. For a complete list of kwargs, see matplotlib.pyplot.plot"""
	return plt.plot(np_disjoint_concatenate(x), np_disjoint_concatenate(y), linespec, **kwargs)[0]


def np_disjoint_concatenate(x):
	"""Concatenate arrays in x separating them with a NaN value, so that they are plotted discontinuously"""
	return reduce(lambda u, v: np.concatenate((u, [np.NaN], v)), x)

