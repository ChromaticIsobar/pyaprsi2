"""Util functions for the module"""
import numpy as np
from collections.abc import Iterable
from itertools import chain, repeat
import wavio
import warnings


def print_parameters(v, condition=lambda x: x[0][0] != "_" and type(x[1]).__name__ not in ("module", "function", "type", "class", "ABCMeta")):
	"""Print every element in the dictionary of v
	
	v : object
		the object of which to print the fields. If v is a dictionary the items of v are printed, otherwise the items in the dict of v
	condition : function
		a condition that fields must comply to in order to be printed
	
	return : str
		a multiline string with the fields of v formatted as '*name* (*type*): *value*'"""
	return "\n".join("{} ({}): {}".format(x[0], type(x[1]).__name__, x[1]) for x in (v if isinstance(v, dict) else v.__dict__).items() if condition(x))


def normalize(a, stat=False):
	"""Normalize the array a
	
	a : numpy.ndarray
		the array to be normalized
	
	stat : bool
		whether to perform statistical normalization (zero-mean and unitary variance) or not (minium is zero and maximum is one)
	
	return : numpy.ndarray
		the normalized array"""
	m, s = (np.nanmean(a), np.nanstd(a)) if stat else (np.nanmin(a), np.nanmax(a) - np.nanmin(a))
	return (a - m)/s


def debias(a):
	"""Remove the DC from a
	
	a : numpy.ndarray
		the array to be debiased
	
	return : numpy.ndarray
		the debiased array"""
	return a - np.nanmean(a)


def match_gain(x, t, start=0, end=-1):
	"""Returns the gain value to matches the energy of x to the one of t in the indices between start and end
	
	x : numpy.ndarray
		the array to amplify or attenuate
	
	t : numpy.ndarray
		the reference array
	
	start : int
		beginning index of comparison region
	
	end : int
		ending index of comparison region
	
	return : double
		the gain"""
	return np.nanstd(t[start:end])/np.nanstd(x[start:end])


def no_zeros(a):
	"""Substitute zeros with NaNs in the array a
	
	a : numpy.ndarray
		the array from which to remove zeros
	
	return : numpy.ndarray
		the array with NaNs instead of zeros"""
	return np.array([no_zeros(elem) for elem in a]) if isinstance(a, Iterable) else (np.NaN if a == 0 else a)


def sdt_synth(impact, n):
	"""Synthesise sound from the SDT Impact object
	
	impact : SDT.Impact
		SDT Impact instance, with the first resonator set to an inertial mass and the second to a full resonator
	
	n : int
		number of samples to generate
	
	return : tuple
		audio samples and final state"""
	tmp = np.zeros(2).astype(np.double)
	return np.array([tmp[1] for v in chain([-1], repeat(0, n - 1)) if impact.dsp(0, v, 0, 0, 0, 0, tmp) is None]), tmp


def sdt_write_to_file(fname, freqs, decays, mags, fragsize):
		with open(fname, "w") as f:
			f.write("\n".join("{}{} {};".format(pname, " 0" if pname == "pickup" else "", " ".join(str(v) for v in pvalue) if isinstance(pvalue, Iterable) else pvalue) for pvalue, pname in zip([freqs, decays, mags, fragsize, freqs.size], ["freqs", "decays", "pickup", "fragmentSize", "activeModes"])))


def mean_freq(ft):
	"""Compute mean non-zero value of ft
	
	ft : numpy.ndarray
		array of frequency values and zeros
	
	return : double
		mean frequency"""
	return np.mean(ft[np.nonzero(ft)[0]])


def ndigits(x):
	"""Compute number of digits of integer x
	
	x : int
		number
	
	return : int
		number of digits of x"""
	return np.ceil(np.log10(x + 1)).astype(int)


def hz2mel(f):
	"""Convert hertz frequencies into mels
	
	f : double
		frequency value in hertz
	
	return : double
		frequency value in mels"""
	return 2595*np.log10(1 + f/700)


def read_file(f):
	"""Read audio file, convert it to mono, and normalize it
	
	f : str
		file path
	
	return : x, fs
		audio samples and sampling frequency"""
	wavf = wavio.read(f)
	fs, x = wavf.rate, wavf.data.astype(np.float32)
	x = x.astype(np.float32)
	if len(x.shape) > 1:
		x = np.mean(x, axis=1)
	x = 2*normalize(x) - 1
	return x, fs


def kebab2camel(s, spacing=False, firstCapital=False):
	"""Convert kebab notation to camel case
	
	s : string
		the string to convert
	
	spacing : bool
		whether to separate words with a space (default is `False`)
	
	firstCapital : bool
		whether to capitalize the first word (default is `False`)"""
	t = [c for c in s.lower()]
	capital = firstCapital
	j = 0
	for c in s:
		if capital:
			t[j] = c.upper()
		capital = False
		if c == "_":
			capital = True
			if spacing:
				t[j] = " "
			else:
				del t[j]
				j -= 1
		j += 1
	return "".join(t)


def batch(it, batch_size):
	"""Generator that returns a list of `batch_size` contiguous elements from `it` at every step. If `it` is
	
	`[it[0], it[1], it[2], ... ]`
	
	Then, `batch(it, batch_size)` is
	
	`[[it[0], it[1], ..., it[batch_size-1]], [it[batch_size], it[batch_size+1], ..., it[2*batch_size-1]], ... ]`
	
	it : Iterable
		Iterable over which to iterate
	
	batch_size : int
		the number of contiguous elements to return at each step
	"""
	it = iter(it)
	loop = True
	while loop:
		cur_batch = [] 
		for _ in range(batch_size):
			try:
				cur_batch.append(next(it))
			except StopIteration:
				loop = False
				break
		if cur_batch:
			yield cur_batch


def array_map(fun, it):
	"""Same as builtin `map` function, but returns the result as a numpy.ndarray instead of a `map` object"""
	return np.array(list(map(fun, it)))


def tryf(foo):
	"""Returns a function that tries to evaluate `foo(val)` and, in case of failure, returns `val`"""
	def footry(val):
		try:
			return foo(val)
		except:
			return val
	return footry
