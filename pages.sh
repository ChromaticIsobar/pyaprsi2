#!/usr/bin/env bash

mkdir -p public/$1 && cp *.$2 public/$1
echo "<!DOCTYPE html><html><body><ul>" > public/$1/index.html
for f in *.$2; do \
	echo "<li><a href=\""$f"\">$f</a></li>" >> public/$1/index.html;
done
echo "</ul></body></html>" >> public/$1/index.html
