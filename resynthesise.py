import SDT
import modalest.model
import modalest.plots
from modalest.limiter import Limiter
import numpy as np
import matplotlib.pyplot as plt
from itertools import chain
import wavio
import os
import sys
import time


def read_file(f):
	if verbose:
		print("Reading file: {}".format(f), end='\r')
	x, fs = modalest.model.read_file(f)
	if verbose:
		print("Read file: {}   ".format(f))
	return x, fs


if __name__ == "__main__" and len(sys.argv) > 1:
	fname = sys.argv[1]
	gain_t = 0.10
	verbose = True
	plot_ = True if len(sys.argv) < 3 else eval(sys.argv[2])
	out_sdt = True
	out_param = True if len(sys.argv) < 4 else eval(sys.argv[3])
	limiter = True if len(sys.argv) < 5 else eval(sys.argv[4])
	target_subs = ("original", "sdtmodaltracker", "out")
	tic = time.time()
	
	# Read target
	x, fs = read_file(fname)
	n = x.size
	SDT.setSampleRate(fs)
	
	# Read SDTModalTracker resynthesis (if present)
	idx = fname.rfind(target_subs[0])
	flag_sdtmt = idx >= 0
	if flag_sdtmt:
		fname_sdtmt = fname[:idx] + target_subs[1] + fname[idx+len(target_subs[0]):]
		x_sdtmt, fs_sdtmt = read_file(fname_sdtmt)
		fname_out = fname[:idx] + target_subs[2] + fname[idx+len(target_subs[0]):]
	else:
		fname_out = fname[:-4] + "_" + target_subs[2] + fname[-4:]
	
	# Fit model
	m = modalest.model.ModalModel(fs=fs, M=2**11, N=2**14, hf=18000, minSineDur=0.02, dt=0.1, mth=-60, mabs=True, get_intermediate=True)
	m.fit(x, verbose=verbose)
	
	# Build structures
	sdt_params = list(m.to_sdt())
	r = SDT.Resonator.fromList(sdt_params[0], sdt_params[1], sdt_params[2]/(1 << 12), sdt_params[3])
	h = SDT.Inertial(mass=0.01)
	imp = SDT.Impact(Stiffness=1e+12, Dissipation=1e+50, Shape=1.5, FirstPoint=0, SecondPoint=0, FirstResonator=h, SecondResonator=r)
	
	# Synthesize audio
	if verbose:
		print("Synthesizing audio...", end='\r')
	out = modalest.model.sdt_synth(imp, n)[0]
	if verbose:
		print("Synthesized audio    ")
	
	# Plot
	if plot_:
		print("Plotting:")
		for i, (res, tit) in enumerate(zip(chain(m.intermediate, [m.sms_tracks(m.intermediate[0][0].shape[0])]), ["SMS analysis tracks", "SMS analysis tracks (split)", "SMS analysis tracks (grouped)", "SMS analysis tracks (after dropping)", "Regression tracks"])):
			print("  {}/{} {}".format(i + 1, len(m.intermediate) + 2, tit))
			plt.gcf().set_size_inches(16, 9)
			plt.gcf().suptitle(tit)
			modalest.plots.plot_freq_and_mag(*res[:2], x=x, fs=m.fs, win=m.w, M=m.M, H=m.H, N=m.N)
			plt.savefig("{} - {} - {}.png".format(fname[:-4], i + 1, tit))
			plt.close()
		
		tit, i = "Audio plots", i + 1
		print("  {}/{} {}".format(i + 1, len(m.intermediate) + 2, tit))
		plt.gcf().set_size_inches(np.array([16, 9])*1.5)
		plt.gcf().suptitle(tit)
		ax = plt.subplot(321 if flag_sdtmt else 221); plt.plot(np.arange(n)/fs, x); plt.grid(True); plt.title('target')
		if flag_sdtmt: plt.subplot(323, sharex=ax, sharey=ax); plt.plot(np.arange(x_sdtmt.size)/fs_sdtmt, x_sdtmt); plt.grid(True); plt.title('SDTModalTracker')
		plt.subplot(325 if flag_sdtmt else 223, sharex=ax, sharey=ax); plt.plot(np.arange(n)/fs, out); plt.grid(True); plt.title('model output')
		ax = plt.subplot(322 if flag_sdtmt else 222); modalest.plots.spectrogram(x=x, fs=m.fs, win=m.w, M=m.M, H=m.H, N=m.N)
		if flag_sdtmt: plt.subplot(324, sharex=ax, sharey=ax); modalest.plots.spectrogram(x=x_sdtmt, fs=fs_sdtmt, win=m.w, M=m.M, H=m.H, N=m.N)
		plt.subplot(326 if flag_sdtmt else 224, sharex=ax, sharey=ax); modalest.plots.spectrogram(x=out, fs=m.fs, win=m.w, M=m.M, H=m.H, N=m.N)
		plt.savefig("{} - {} - {}.png".format(fname[:-4], i + 1, tit))
		plt.close()
		
	# Save parameters
	if out_param:
		pfname = os.path.join(os.path.dirname(fname), "params_{}.sdt".format(os.path.basename(fname)[:-4]))
		if verbose:
			print("Writing parameters to file: '{}'".format(pfname), end='\r')
		modalest.model.sdt_write_to_file(pfname, *sdt_params)
		if verbose:
			print("Wrote parameters to file: '{}'   ".format(pfname))
	
	# Save audio
	if out_sdt:
		if verbose:
			print("Writing audio output to file: {}".format(fname_out), end='\r')
		wavio.write(fname_out, out, fs, sampwidth=3)
		if verbose:
			print("Wrote audio output to file: {}  ".format(fname_out))
		if limiter:
			out_limiter, fname_out_limiter = Limiter(threshold=10**(-0.03/20), release=10**(-0.005/20), attack=0.2)(out), fname[:idx] + target_subs[2] + "_limit" + fname[idx+len(target_subs[0]):]
			if verbose:
				print("Writing audio output to file: {}".format(fname_out_limiter), end='\r')
			wavio.write(fname_out_limiter, out_limiter, fs, sampwidth=3)
			if verbose:
				print("Wrote audio output to file: {}  ".format(fname_out_limiter))
	
	toc = time.time()
	if verbose:
		print("Elapsed time: {} seconds".format(int(toc - tic)))
