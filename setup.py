import sys
import os


python = "python" if len(sys.argv) < 2 else sys.argv[1]
if os.path.exists("sms/__init__.py"):
	print("skipping setup of sms ('sms/__init__.py' found)")
else:
	os.system("cd sms && {0} setup.py {0}".format(python))
os.system("cd pysdt && {0} setup.py build_ext --inplace && cp *.so ..".format(python))
