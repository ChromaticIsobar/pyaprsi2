import numpy as np
import matplotlib.pyplot as plt


if __name__ == "__main__":
	plt.xkcd()
	fs = 44100
	f = 4
	dur = 1
	d = 0.4
	m = 0
	dim = 1
	
	a = lambda t: np.exp(m-t/d)
	e = lambda t: np.cos(2*np.pi*f*t)
	x = lambda t: a(t)*e(t)
	
	n = int(fs*dur)
	t = np.arange(n)/fs
	
	plt.gcf().set_size_inches(np.array([1, 1])*dim)
	plt.plot(t, np.array([a(t), -a(t)]).T, '--', color='orange')
	plt.plot(t, x(t))
	plt.xticks([])
	plt.yticks([])
	plt.suptitle("x(t)", fontsize=10)
	plt.savefig("thumb_{}in.png".format(dim))
	plt.savefig("thumb_{}in.svg".format(dim))
	plt.show()
