#!/usr/bin/env bash

# 'running', 'success' or 'failure' is in this file
STATUS=`cat status.txt`
STAGE=`cat stage.txt`

# Set values for shields.io fields based on STATUS
if [ $STATUS = "pending" ]; then
	BADGE_COLOR="lightgray"
elif [ $STATUS = "running" ]; then
	BADGE_COLOR="yellow"
elif [ $STATUS = "publishing" ]; then
	BADGE_COLOR="lightblue"
elif [ $STATUS = "failed" ]; then
	BADGE_COLOR="red"
elif [ $STATUS = "online" ]; then
	BADGE_COLOR="brightgreen"
elif [ $STATUS = "success" ]; then
	BADGE_COLOR="brightgreen"
else
	exit 1
fi

# Set filename for the badge (i.e. 'ci-test-branch-job.svg')
BADGE_FILENAME="ci-$STAGE-$CI_COMMIT_REF_NAME.svg"

# Get the badge from shields.io
SHIELDS_IO_NAME=$STAGE-$STATUS-$BADGE_COLOR.svg
echo "INFO: Fetching badge $SHIELDS_IO_NAME from shields.io to $BADGE_FILENAME."
curl "https://img.shields.io/badge/$SHIELDS_IO_NAME" > $BADGE_FILENAME

# Upload it to Dropbox (DBOX_TOKEN and DBOX_BADGES_PATH are secret CI variables)
echo "INFO: Uploading badge to Dropbox."
curl -s -X POST https://content.dropboxapi.com/2/files/upload \
  --header "Authorization: Bearer $DBOX_TOKEN" \
  --header "Content-Type: application/octet-stream" \
  --header "Dropbox-API-Arg: {\"path\":\"/$DBOX_BADGES_PATH/$BADGE_FILENAME\",\"autorename\":false,\"mute\":false,\"mode\":{\".tag\":\"overwrite\"}}" \
  --data-binary @"$BADGE_FILENAME"
